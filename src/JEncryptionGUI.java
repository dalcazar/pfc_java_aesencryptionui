import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class JEncryptionGUI {

	private JFrame frame;
	private JTextField tfInputE;
	private JTextField tfKey;
	private JTextField tfOutputE;
	private JTextField tfInputD;
	private JTextField tfOutputD;

	private static final String ALGO = "AES/ECB/NoPadding";
	private static final byte[] keyValue = new byte[] { 'T', 'h', 'e', 'B',
			'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y' };
	private static JTextField tfOutputEH;
	private static JTextField tfOutputDH;
	
	private static Charset utf8charset = Charset.forName("UTF-8");
	private static Charset iso88591charset = Charset.forName("UTF-16");
	
	/*// decode UTF-8
	CharBuffer data = utf8charset.decode(inputBuffer);

	// encode ISO-8559-1
	ByteBuffer outputBuffer = iso88591charset.encode(data);
	byte[] outputData = outputBuffer.array();*/
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JEncryptionGUI window = new JEncryptionGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
		

	/**
	 * Create the application.
	 */
	public JEncryptionGUI() {
		initialize();

		tfKey.setText(toHex(keyValue.toString()));
		//tfKey.setText(keyValue.toString());

		tfInputE.setText("abcdefghijklmnop");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 693, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("AES", null, panel_1, null);
		panel_1.setLayout(null);

		JButton btnEncrypt = new JButton("Encrypt");
		btnEncrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String data = tfInputE.getText();
				try {
					String dataEnc = Encrypt(data);
					tfOutputE.setText(dataEnc);
					tfOutputEH.setText(toHex(dataEnc));
					tfInputD.setText(dataEnc);
				} catch (Exception e) {
					tfOutputE.setText("ERROR");
				}
			}
		});
		btnEncrypt.setBounds(50, 169, 301, 54);
		panel_1.add(btnEncrypt);

		tfInputE = new JTextField();
		tfInputE.setBounds(50, 14, 301, 20);
		tfInputE.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(tfInputE);
		tfInputE.setColumns(10);

		JLabel lblInput = new JLabel("Input");
		lblInput.setBounds(10, 17, 46, 14);
		panel_1.add(lblInput);

		JLabel lblKey = new JLabel("Key");
		lblKey.setBounds(10, 42, 46, 14);
		panel_1.add(lblKey);

		JLabel lblOutput = new JLabel("Output");
		lblOutput.setBounds(10, 84, 46, 14);
		panel_1.add(lblOutput);

		tfKey = new JTextField();
		tfKey.setHorizontalAlignment(SwingConstants.CENTER);
		tfKey.setBounds(50, 39, 612, 20);
		panel_1.add(tfKey);
		tfKey.setColumns(10);

		tfOutputE = new JTextField();
		tfOutputE.setHorizontalAlignment(SwingConstants.CENTER);
		tfOutputE.setBounds(50, 81, 301, 20);
		panel_1.add(tfOutputE);
		tfOutputE.setColumns(10);

		JButton btnDecrypt = new JButton("Decrypt");
		btnDecrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String data = tfInputD.getText();
				try {
					String dataDec = Decrypt(data);
					tfOutputD.setText(dataDec);
					tfOutputDH.setText(toHex(dataDec));
				} catch (Exception ee) {
					tfOutputD.setText("ERROR");
				}
			}
		});
		btnDecrypt.setBounds(361, 169, 301, 54);
		panel_1.add(btnDecrypt);

		tfInputD = new JTextField();
		tfInputD.setHorizontalAlignment(SwingConstants.CENTER);
		tfInputD.setBounds(361, 14, 301, 20);
		panel_1.add(tfInputD);
		tfInputD.setColumns(10);

		tfOutputD = new JTextField();
		tfOutputD.setHorizontalAlignment(SwingConstants.CENTER);
		tfOutputD.setColumns(10);
		tfOutputD.setBounds(361, 81, 301, 20);
		panel_1.add(tfOutputD);
		
		tfOutputEH = new JTextField();
		tfOutputEH.setBounds(50, 109, 301, 20);
		panel_1.add(tfOutputEH);
		tfOutputEH.setColumns(10);
		
		tfOutputDH = new JTextField();
		tfOutputDH.setColumns(10);
		tfOutputDH.setBounds(361, 109, 301, 20);
		panel_1.add(tfOutputDH);
		
		JLabel lblHex = new JLabel("HEX");
		lblHex.setBounds(10, 112, 46, 14);
		panel_1.add(lblHex);

		JPanel panel = new JPanel();
		tabbedPane.addTab("HMAC-SHA256", null, panel, null);
	}

	private static String Encrypt(String Data) throws Exception {
		Key key = GenerateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes(Charset.defaultCharset()));
		return new String(encVal, Charset.defaultCharset());
	}

	private static String Decrypt(String encryptedData) throws Exception {
		Key key = GenerateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decValue = c.doFinal(encryptedData.getBytes(Charset.defaultCharset()));
		return new String(decValue, Charset.defaultCharset());
	}

	private static Key GenerateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, "AES");
		return key;
	}
	
	private static String toHex(String arg) {
	    return String.format("%040x", new BigInteger(arg.getBytes(Charset.defaultCharset())));
	}
}
